import QtQuick 2.3
import QtQuick.Window 2.2

Window {
    id: mainwindow

    visible: true
    width: 400
    height: 400

    property int workLength: 60*60
    property int workLeft: workLength

    property int breakLength: 60*4
    property int breakLeft: breakLength

    property bool inBreak: false

    function getAttention() {
        var w = mainwindow;
        w.setX(Screen.width / 2 - w.width / 2);
        w.setY(Screen.height / 2 - w.height / 2);
        w.alert(0);
    }

    function takeBreak() {
        worktimer.stop();
        breaktimer.start();
        mainwindow.workLeft = 0;
        mainwindow.breakLeft = mainwindow.breakLength;
        inBreak = true;
        mainwindow.showFullScreen();
    }

    function startWork() {
        mainwindow.workLeft = mainwindow.workLength;
        breaktimer.stop();
        worktimer.start();
        inBreak = false;
        mainwindow.showNormal();
    }

    Component.onCompleted: startWork()

    onWorkLeftChanged: {
        if (workLeft > 0) return;

        worktimer.stop();
        getAttention();
    }

    onBreakLeftChanged: {
        if (breakLeft > 0) return;

        startWork();
    }

    Timer {
        id: worktimer
        interval: 1000
        running: true
        repeat: true
        onTriggered: workLeft = workLeft - 1
    }

    Timer {
        id: breaktimer
        interval: 1000
        running: false
        repeat: true
        onTriggered: breakLeft = breakLeft - 1
    }

    Item {
        focus: true
        Keys.onEscapePressed: {
            console.log('esc')
            startWork();
        }
    }

    Rectangle {
        id: workrect
        visible: !mainwindow.inBreak
        anchors.fill: parent
        color: '#357'
        Column {
            anchors.centerIn: parent
            width: breaktext.width + 40
            Item {
                width: parent.width
                height: timertext.height
                Text {
                    anchors.centerIn: parent
                    id: timertext
                    font.pixelSize: 38
                    visible: mainwindow.workLeft > 0
                    color:  mainwindow.workLeft < 60 ? 'red' : 'silver'
                    text: " " + Math.floor(mainwindow.workLeft / 60).toString() +
                          ':' + ('0' + (mainwindow.workLeft % 60)).slice(-2) + " "
                }
            }

            Rectangle {
                color: breakbutton.containsMouse ? '#568' : workrect.color
                width: parent.width
                height: timertext.visible ? 20 : 150
                Text {
                    id: breaktext
                    anchors.centerIn: parent
                    font.pixelSize: timertext.visible ? 12 : 42
                    text: "Take a break"
                    MouseArea {
                        id: breakbutton
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: takeBreak()
                    }
                }
            }
        }
    }

    Rectangle {
        id: breakrect
        visible: mainwindow.inBreak
        color: 'black'
        anchors.fill: parent

        Column {
            id: messagecol
            anchors.centerIn: parent
            width: 300
            Rectangle {
                color: '#a00'
                radius: 10
                width: parent.width - 50
                x: 25
                height: 80
                Text {
                    anchors.centerIn: parent
                    text: 'STOP!'
                    color: 'grey'
                    font.pixelSize: 40
                }
            }
            Item {
                width: parent.width
                height: 50
                Text {
                    anchors.centerIn: parent
                    color: 'silver'
                    font.pixelSize: 50
                    text: 'Step away'
                }
            }
            Item {
                width: parent.width
                height: 35
                Text {
                    anchors.centerIn: parent
                    color: 'silver'
                    font.pixelSize: 30
                    text: 'from the keyboard'
                }
            }
            Item {
                width: parent.width
                height: 30
                Text {
                    anchors.centerIn: parent
                    color: 'orange'
                    font.pixelSize: 24
                    text: mainwindow.breakLeft
                }
            }
        }
    }
}
